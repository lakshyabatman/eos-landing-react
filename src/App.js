import React from 'react'
import ContributingGuide from './pages/contributing-guide'

function App() {
  return <ContributingGuide />
}

export default App
