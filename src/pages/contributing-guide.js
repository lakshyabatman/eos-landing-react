import React from 'react'
import HeaderComponent from '../components/header'
import FooterComponent from '../components/footer'

const ContributingGuide = () => {
  return (
    <div>
      {/* NavBar UI Component */}
      <div className='container'>
        <HeaderComponent />
        {/* 
        We can divide section into multiple UI components
        */}
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id mi
          finibus metus molestie dapibus. Nam rhoncus ante rhoncus libero
          varius, vel imperdiet enim pellentesque. Ut vestibulum urna non lorem
          efficitur, sed tincidunt velit dictum. In porta arcu at quam blandit,
          at pharetra purus interdum. Integer vitae elementum ipsum. In
          sollicitudin erat sed orci condimentum, sit amet egestas felis
          faucibus. Fusce at elit ligula. Praesent malesuada augue molestie
          scelerisque commodo. Donec egestas semper nulla, ullamcorper eleifend
          magna gravida nec. Sed tempor massa et euismod gravida. Suspendisse
          imperdiet ipsum id tellus sollicitudin consectetur. Nulla facilisi.
          Quisque euismod velit ut massa consequat, non elementum turpis
          convallis.
        </p>
        <p>
          Mauris ut turpis quis dui lobortis pharetra a sit amet risus. Quisque
          dignissim ipsum a felis egestas, ut facilisis augue pellentesque.
          Mauris ultricies, mi at lacinia faucibus, augue felis maximus est, at
          auctor nisi sem et ipsum. Maecenas ac ultricies justo. Praesent
          ultricies odio tincidunt massa viverra sollicitudin. Vivamus risus
          leo, pretium ut pretium et, mattis condimentum nisl. Donec accumsan
          augue non erat tincidunt fermentum. Fusce orci tellus, viverra id
          purus et, molestie cursus libero. Morbi rhoncus metus in finibus
          ultricies. Integer eu ullamcorper lorem, gravida lobortis felis. Sed
          lobortis, ante dictum pulvinar venenatis, risus nisl ultrices arcu, id
          ornare mauris nisl eget metus. Cras nunc nisi, sollicitudin eget
          fermentum sit amet, finibus mattis metus.
        </p>
        <p>
          Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus
          tempus ac massa eu laoreet. Pellentesque commodo mattis orci in
          luctus. Donec lacinia, odio et blandit tincidunt, eros nunc dapibus
          orci, eu tincidunt sem enim sit amet urna. Morbi pellentesque ut felis
          a dapibus. Donec varius convallis risus nec interdum. Nunc libero
          mauris, tincidunt nec ex non, aliquam aliquam mi. Aliquam auctor ipsum
          sem, vehicula vestibulum purus placerat quis. Quisque vel faucibus
          nunc, ut ullamcorper justo.
        </p>
        <p>
          Sed in suscipit ligula. Etiam ullamcorper laoreet sapien, ut auctor
          nunc. Nunc interdum, dolor non scelerisque rhoncus, ex quam pulvinar
          purus, vitae faucibus arcu purus non est. Vestibulum commodo elementum
          magna, et lacinia ante venenatis vel. In tincidunt, leo non tristique
          sollicitudin, lectus arcu sollicitudin est, sit amet efficitur leo
          enim ac nulla. Duis arcu quam, ornare sit amet sodales non, ultrices
          sed ex. Integer varius scelerisque molestie. Mauris quis fringilla
          elit, sit amet tincidunt urna. Vestibulum et sagittis justo. Cras nec
          feugiat lacus. Nulla facilisi. Proin nunc tellus, ullamcorper nec
          egestas non, dictum eget nulla. Donec ut aliquet odio. Maecenas mollis
          ex nec eleifend varius.
        </p>
        <p>
          Cras quis lacinia risus, vel consequat nunc. Praesent ut turpis
          semper, semper urna non, auctor augue. Pellentesque facilisis justo id
          auctor consequat. Nam a dapibus orci, sit amet luctus sem. Donec enim
          massa, interdum et ligula non, suscipit pulvinar nunc. Fusce id cursus
          diam, eu interdum sapien. Nulla pretium, lectus vehicula imperdiet
          lobortis, ex arcu imperdiet orci, in ultrices orci lectus ac ipsum.
          Fusce vel ligula volutpat, dictum lorem non, malesuada turpis.
          Phasellus mattis hendrerit venenatis. Donec tincidunt elit justo, ac
          eleifend purus fermentum at. Aliquam sagittis velit a luctus semper.
          Sed laoreet lectus quis nunc iaculis, vel placerat massa pellentesque.
          Sed eget consequat nisi. Integer viverra ut augue et rutrum. Quisque
          nec dictum risus.
        </p>
      </div>
      <FooterComponent />
    </div>
  )
}

export default ContributingGuide
